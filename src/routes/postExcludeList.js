// @flow
'use strict'

const Day = require('../lib/Day')

module.exports = (db: Object) => (x: Object, next: Function) => {
  if (x.method !== 'POST' || x.url !== '/excludelist') {
    return next()
  }

  const query: Array<Day> = x.request.body

  if (!Array.isArray(query) || query.some(q => !Day.isValid(q))) {
    x.status = 400
    x.message = 'Invalid query, bad request'
    x.body = {
      error: true,
      status: x.status,
      message: x.message
    }
    return x.body
  }

  db.set('excludeList', query)

  x.status = 200

  x.body = {
    ok: true,
    status: x.status
  }

  return x.body
}
