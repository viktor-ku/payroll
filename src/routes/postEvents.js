// @flow
'use strict'

type Response = {
  data?: Array<Object>,
  error?: boolean,
  ok?: boolean,
  status: number,
  errors?: Array<string>
}

const Parser = require('../lib/Parser')

module.exports = (db: Object) => (x: Object, next: Function): Response => {
  if (x.method !== 'POST' || x.url !== '/events') {
    return next()
  }

  const query = x.request.body
  const valid = Parser.validate(query)

  if (valid.error) {
    x.status = 400

    x.body = {
      status: x.status,
      error: valid.error,
      errors: valid.errors.map(x => x.message)
    }

    return x.body
  }

  const excludeList = db.get('excludeList')
  const events = Parser.parse(query, excludeList)

  x.status = 200

  x.body = {
    ok: true,
    status: 200,
    data: events
  }

  return x.body
}
