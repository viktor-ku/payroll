// @flow
'use strict'

module.exports = (db: Object) => (x: Object, next: Function) => {
  if (x.method !== 'GET' || x.url !== '/excludelist') {
    return next()
  }

  x.body = db.get('excludeList')

  return x.body
}
