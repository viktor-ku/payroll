// @flow
'use strict'

const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const config = require('../config.json')

const routes = {}
const state = new Map()
const app = new Koa()

routes['get:excludelist'] = require('./routes/getExcludeList')
routes['post:excludelist'] = require('./routes/postExcludeList')
routes['post:events'] = require('./routes/postEvents')

state.set('excludeList', config.exclude)

app
  .use(routes['get:excludelist'](state))
  .use(bodyParser())
  .use(routes['post:events'](state))
  .use(routes['post:excludelist'](state))

app.listen(config.port, () => {
  console.log('Server listening at http://localhost:%s', config.port)
})
