FROM node:8-alpine

EXPOSE 8080

ENV NODE_ENV production
ENV WORKDIR /home/node/app
WORKDIR $WORKDIR

COPY dist dist
COPY package.json package.json
COPY package-lock.json package-lock.json
COPY config.json config.json

RUN npm install

RUN chown -R node:node /home/node/app

USER node

CMD npm start
