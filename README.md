# Payroll

> Payroll demo project

## Default exclude list

### Set

Default exclude list you can either set in `config.json` file at the root of the project or set it by `POST /excludelist` route

You may need to set default exclude list for all incoming data. It can be used for:

- Company Holidays
- Bank holidays
- Some other type of excludes that you want to apply to **everything**

**Note** you will override previously set `excludelist`

```bash
POST /excludelist
```

with query

```js
[
  {
    "year": 2017,
    "month": 6,
    "day": [1, 2, 3, 4, 5, 6, 7, 8]
  },
  {
    "year": 2017,
    "month": 7,
    "day": [15, 16]
  }
]
```

- for each month separate records are required
- you can set multiple days within one month by placing them into array
- or you can use just single day as a number value

### Get

```bash
GET /excludelist
```

Get the current exclude list. As a result you will get either an empty array or array with objects as you set it

## Processing

```bash
POST /events
```

with query

```js
{
  "employee": "employee-id",
  "company": "company-id",
  "from": { "year": 2017, "month": 6, "day": 1 },
  "to": { "year": 2017, "month": 6, "day": 30 },
  exclude: []
}
```

- as exclude field you can pass any number of days to exclude in the same manner as for `excludelist` you did
- as a result you will get object with meta, errors and result fields
- note that you can't use arrays as a day type in `from` and `to` fields
