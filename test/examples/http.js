// @flow
'use strict'

const t = require('tap')
const request = require('request-promise')
const config = require('../../config.json')

t.test('example', async t => {
  let response

  response = await getExcludeList()

  t.deepEqual(response, config.exclude)

  response = await postEvents({
    company: 'Awesome company',
    employee: 'Michael Jacobs',
    from: { year: 2017, month: 6, day: 23 },
    to: { year: 2017, month: 6, day: 25 }
  })

  t.equal(response.ok, true)
  t.equal(response.status, 200)
  t.ok(Array.isArray(response.data))
  t.equal(response.data.length, 1)

  response = await postExcludeList([])

  t.equal(response.ok, true)
  t.equal(response.status, 200)

  response = await getExcludeList()

  t.deepEqual(response, [])

  response = await postEvents({
    company: 'Awesome company',
    employee: 'Michael Jacobs',
    from: { year: 2017, month: 6, day: 23 },
    to: { year: 2017, month: 6, day: 25 }
  })

  t.equal(response.ok, true)
  t.equal(response.status, 200)
  t.ok(Array.isArray(response.data))
  t.equal(response.data.length, 3)

  response = await postEvents({})

  t.equal(response.error, true)
  t.equal(response.status, 400)
  t.ok(Array.isArray(response.errors))
  t.equal(response.errors.length, 4)

  t.end()
})

function postExcludeList (payload: Array<Object>) {
  return request({
    method: 'POST',
    uri: `http://localhost:${config.port}/excludelist`,
    body: payload,
    json: true
  })
}

function postEvents (payload: Object) {
  return request({
    method: 'POST',
    uri: `http://localhost:${config.port}/events`,
    body: payload,
    simple: false,
    json: true
  })
}

function getExcludeList () {
  return request({
    method: 'GET',
    uri: `http://localhost:${config.port}/excludelist`,
    json: true
  })
}
