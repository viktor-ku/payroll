// @flow
'use strict'

const t = require('tap')
const sinon = require('sinon')
const postExcludeList = require('../../src/routes/postExcludeList')

t.test('postExcludeList', t => {
  t.test('ok', t => {
    const state = new Map()
    const query = [
      {
        'year': 2017,
        'month': 6,
        'day': [1, 2, 3, 4, 5, 6, 7, 8]
      },
      {
        'year': 2017,
        'month': 7,
        'day': [15, 16]
      }
    ]
    state.set('excludeList', [])

    const x = {
      method: 'POST',
      url: '/excludelist',
      status: 404, // koa.js default is 404
      request: {
        body: query
      }
    }
    const next = sinon.stub()

    const response = postExcludeList(state)(x, next)

    t.equal(next.callCount, 0)
    t.equal(x.status, 200)
    t.deepEqual(query, state.get('excludeList'))
    t.deepEqual(response, {
      status: 200,
      ok: true
    })

    t.end()
  })

  t.test('invalid query', t => {
    const state = new Map()
    const query = [
      {
        'year': 2017,
        'month': 7,
        'day': []
      }
    ]
    const x = {
      method: 'POST',
      url: '/excludelist',
      status: 404,
      message: 'not found',
      next: sinon.stub(),
      request: {
        body: query
      }
    }
    const next = sinon.stub()

    state.set('excludeList', [])

    const response = postExcludeList(state)(x, next)

    t.equal(next.callCount, 0, 'next count')
    t.equal(x.status, 400)
    t.equal(x.message, 'Invalid query, bad request')
    t.deepEqual(response, {
      status: 400,
      error: true,
      message: 'Invalid query, bad request'
    })

    t.end()
  })

  t.test('call next', t => {
    const x = {
      method: 'POST',
      url: '/excludelist-list-invalid-route'
    }
    const next = sinon.stub()

    postExcludeList(new Map())(x, next)

    t.equal(next.callCount, 1)
    t.end()
  })

  t.end()
})
