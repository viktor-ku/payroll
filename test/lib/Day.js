// @flow
'use strict'

const t = require('tap')
const Day = require('../../src/lib/Day')

t.test('Day', t => {
  t.test('constructor', t => {
    t.test('ok', t => {
      t.notThrow(() => new Day({ year: 2017, month: 1, day: 1 }))
      t.notThrow(() => new Day({ year: 2017, month: 12, day: 31 }))
      t.end()
    })

    t.test('not ok', t => {
      t.throw(() => new Day({ year: 2017, month: 0, day: 1 }))
      t.throw(() => new Day({ year: 2017, month: 1, day: [1, 2, 3] }))
      t.end()
    })

    t.end()
  })

  t.test('monthLength', t => {
    const year = 2017
    const monthMap = {
      '1': 31,
      '2': 28,
      '3': 31,
      '4': 30,
      '5': 31,
      '6': 30,
      '7': 31,
      '8': 31,
      '9': 30,
      '10': 31,
      '11': 30,
      '12': 31
    }
    const months = Object.keys(monthMap)

    for (let n = 0, len = months.length; n < len; n++) {
      const month: number = Number(months[n])
      const days: number = monthMap[month]
      const totalDays = Day.monthLength(year, month)
      t.equal(days, totalDays, `${month} month is ${days} days long`)
    }

    t.end()
  })

  t.test('equals', t => {
    t.test('ok', t => {
      const a = new Day({ year: 2017, month: 11, day: 1 })
      const b = new Day({ year: 2017, month: 11, day: 1 })

      t.ok(a.equals(b))
      t.ok(b.equals(a))
      t.ok(Day.equals(a, b))
      t.ok(Day.equals(b, a))

      t.end()
    })

    t.test('not ok', t => {
      const a = new Day({ year: 2017, month: 10, day: 6 })
      const b = new Day({ year: 2017, month: 3, day: 19 })

      t.notOk(a.equals(b))
      t.notOk(b.equals(a))
      t.notOk(Day.equals(a, b))
      t.notOk(Day.equals(b, a))

      t.end()
    })

    t.end()
  })

  t.test('clone', t => {
    const day = new Day({ year: 2017, month: 10, day: 6 })
    const clone = Day.clone(day)

    t.ok(day.equals(clone))
    t.notOk(day === clone)

    day.addDay()

    t.notOk(day.equals(clone))

    t.end()
  })

  t.test('add', t => {
    t.test('year', t => {
      const day = new Day({ year: 2017, month: 11, day: 1 })

      day.addYear()
      t.equal(day.month, 11)
      t.equal(day.day, 1)
      t.equal(day.year, 2018)

      day.addYear(10)
      t.equal(day.month, 11)
      t.equal(day.day, 1)
      t.equal(day.year, 2028)

      t.end()
    })

    t.test('month', t => {
      const day = new Day({ year: 2017, month: 6, day: 1 })

      day.addMonth(6)
      t.equal(day.month, 12)
      t.equal(day.day, 1)
      t.equal(day.year, 2017)

      day.addMonth()
      t.equal(day.month, 1)
      t.equal(day.year, 2018)
      t.equal(day.day, 1)

      t.end()
    })

    t.test('day', t => {
      const day = new Day({ year: 2017, month: 11, day: 5 })

      day.addDay(25)
      t.equal(day.day, 30)
      t.equal(day.month, 11)
      t.equal(day.year, 2017)

      day.addDay()
      t.equal(day.day, 1)
      t.equal(day.month, 12)
      t.equal(day.year, 2017)

      day.addDay(31)
      t.equal(day.day, 1)
      t.equal(day.month, 1)
      t.equal(day.year, 2018)

      t.end()
    })

    t.end()
  })

  t.test('get key', t => {
    const day = new Day({ year: 2017, month: 3, day: 10 })
    t.equal(day.toString(), '2017-3-10')
    t.end()
  })

  t.test('isValid', t => {
    t.test('ok', t => {
      t.ok(Day.isValid({ year: 2017, month: 1, day: 14 }))
      t.ok(Day.isValid({ year: 2017, month: 6, day: [1, 2, 3] }))
      t.end()
    })

    t.test('not ok', t => {
      t.notOk(Day.isValid())
      t.notOk(Day.isValid('banana'))
      t.notOk(Day.isValid(null))
      t.notOk(Day.isValid([1, 2, 3]))
      t.notOk(Day.isValid({ month: 1, day: 1 }))
      t.notOk(Day.isValid({ year: 2010, day: 1 }))
      t.notOk(Day.isValid({ year: 2010, month: 1 }))
      t.notOk(Day.isValid({ year: '2017', month: 1, day: 1 }))
      t.notOk(Day.isValid({ year: 2017, month: 0, day: 5 }))
      t.notOk(Day.isValid({ year: 2017, month: 6, day: 42 }))
      t.notOk(Day.isValid({ year: 2017, month: 6, day: [55, 3, 109] }))
      t.notOk(Day.isValid({ year: 2017, month: 6, day: [] }))
      t.end()
    })

    t.end()
  })

  t.test('isValidOne', t => {
    t.test('ok', t => {
      t.ok(Day.isValidOne({ year: 2017, month: 1, day: 14 }))
      t.end()
    })

    t.test('not ok', t => {
      t.notOk(Day.isValidOne())
      t.notOk(Day.isValidOne('banana'))
      t.notOk(Day.isValidOne(null))
      t.notOk(Day.isValidOne([1, 2, 3]))
      t.notOk(Day.isValidOne({ month: 1, day: 1 }))
      t.notOk(Day.isValidOne({ year: 2010, day: 1 }))
      t.notOk(Day.isValidOne({ year: 2010, month: 1 }))
      t.notOk(Day.isValidOne({ year: '2017', month: 1, day: 1 }))
      t.notOk(Day.isValidOne({ year: 2017, month: 0, day: 5 }))
      t.notOk(Day.isValidOne({ year: 2017, month: 6, day: 42 }))
      t.notOk(Day.isValidOne({ year: 2017, month: 6, day: [1, 2, 3] }))
      t.end()
    })

    t.end()
  })

  t.test('split', t => {
    t.test('multiple', t => {
      const days = Day.split({ year: 2017, month: 6, day: [1, 2, 3, 4] })

      t.ok(Array.isArray(days))
      t.equal(days.length, 4)
      t.ok(days.some(x => x.day === 1))
      t.ok(days.some(x => x.day === 2))
      t.ok(days.some(x => x.day === 3))
      t.ok(days.some(x => x.day === 4))
      t.ok(days.every(x => x instanceof Day))

      t.end()
    })

    t.test('single', t => {
      const days = Day.split({ year: 2017, month: 6, day: 13 })

      t.ok(Array.isArray(days))
      t.equal(days.length, 1)
      t.ok(days.some(x => x.day === 13))
      t.ok(days.every(x => x instanceof Day))

      t.end()
    })

    t.test('wrong', t => {
      const days = Day.split({ year: 2017, month: 6, day: [0, 0, 0, 0] })

      t.ok(Array.isArray(days))
      t.equal(days.length, 0)

      t.end()
    })

    t.end()
  })

  t.end()
})
