// @flow
'use strict'

const t = require('tap')
const getId = require('../../src/lib/getId')

t.test('getId', t => {
  const id = getId()

  t.equal(typeof id, 'string')
  t.equal(id.length, 16)

  t.end()
})
